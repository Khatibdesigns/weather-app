//
//  WeatherData.swift
//  Weather - Nader
//
//  Created by Khatib Designs on 2/9/19.
//  Copyright © 2019 Khatib Designs. All rights reserved.
//

import Foundation

class WeatherDataDetails {
    
    var sunrise = ""
    var sunset =  ""
    var humidity = ""
    var pressure = ""
    var wind_speed = ""
    var longtitude = ""
    var latitude = ""
    var temperature = Double()
    var city = ""
    var country = ""
    
    init(sunrise: String, sunset: String, humidity: String, pressure: String, wind_speed: String, longtitude: String, latitude: String, temperature: Double, city: String, country: String) {
        
        self.sunrise = sunrise
        self.sunset = sunset
        self.humidity = humidity
        self.pressure = pressure
        self.wind_speed = wind_speed
        self.longtitude = longtitude
        self.latitude = latitude
        self.temperature = temperature
        self.city = city
        self.country = country
    }
}
