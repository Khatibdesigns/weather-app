//
//  APIClient.swift
//  Weather - Nader
//
//  Created by Khatib Designs on 2/9/19.
//  Copyright © 2019 Khatib Designs. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import CoreData

class APIClient {
    
    var BASE_URL = "https://api.openweathermap.org/data/2.5/weather?"  // OpenWeather URL
    var API_KEY = "69a37252518111bafbc4f52f8db4fcf5" // OpenWeather API Key
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    /**
     Get Online Weather For Geo Location
     */
    func localWeather(withParameters parameters: [AnyHashable : String]?, completion: @escaping (([WeatherDataDetails]) -> Void), failure: @escaping (() -> Void)) {
        var weather_array: [WeatherDataDetails] = [WeatherDataDetails]()
        let lat = "\(parameters!["lat"]!)"
        let long = "\(parameters!["long"]!)"
        
        Alamofire.request(URL(string: "\(BASE_URL)lat=\(lat)&lon=\(long)&APPID=\(API_KEY)")!, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { (response) in
            if let jsonValue = response.result.value {
                let json = JSON(jsonValue)
                
                if "\(json["error"])" == "404" {
                    failure()
                    return
                }
                
                let Data = WeatherDataDetails(
                    sunrise: "\(Date(timeIntervalSince1970: json["sys"]["sunrise"].double ?? 0))",
                    sunset: "\(Date(timeIntervalSince1970: json["sys"]["sunset"].double ?? 0))",
                    humidity: "\(json["main"]["humidity"])%",
                    pressure: "\(json["main"]["pressure"])hpa",
                    wind_speed: "\(json["wind"]["speed"])mp/h",
                    longtitude: "\(json["coord"]["lon"])",
                    latitude: "\(json["coord"]["lat"])",
                    temperature: round(json["main"]["temp"].double ?? 0),
                    city: "\(json["name"])",
                    country: "\(json["sys"]["country"])")
                
                weather_array.append(Data)
                completion(weather_array)
            }
        }
    }
    
    /**
     Get Offline Weather For Geo Location
     */
    func offlineLocalWeather(withParameters parameters: [AnyHashable : String]?, completion: @escaping (([WeatherDataDetails]) -> Void), failure: @escaping (() -> Void)) {
        var weather_array: [WeatherDataDetails] = [WeatherDataDetails]()
        let city = "\(parameters!["city"]!)"
        
        let context = getContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalWeatherData")
        request.returnsObjectsAsFaults = false
        
        let predicate = NSPredicate(format: "city = '\(city)'")
        request.predicate = predicate
        do {
            let result = try context.fetch(request)
            
            if result.count == 0 {
                failure()
                return
            }
            for data in result as! [NSManagedObject] {
                
                let Data = WeatherDataDetails(
                    sunrise: "",
                    sunset: "",
                    humidity: "",
                    pressure: "",
                    wind_speed: "",
                    longtitude: "\(data.value(forKey: "longtitude")!)",
                    latitude: "\(data.value(forKey: "lattitude")!)",
                    temperature: data.value(forKey: "temperature") as! Double,
                    city: "\(data.value(forKey: "city")!)",
                    country: "\(data.value(forKey: "country")!)")
                
                weather_array.append(Data)
                completion(weather_array)
            }
        }catch{
            failure()
        }
    }
    
    /**
     Add Offline Weather Data To Core
     */
    func writeOfflineLocalWeather(withParameters parameters: [AnyHashable : Any]?, completion: @escaping (() -> Void), failure: @escaping (() -> Void)) {
        
        let lat = "\(parameters!["lat"]!)"
        let long = "\(parameters!["long"]!)"
        let city = "\(parameters!["city"]!)"
        let country = "\(parameters!["country"]!)"
        let temp = parameters!["temp"] as! Double
        
        let context = self.getContext()
        let entity =  NSEntityDescription.entity(forEntityName: "LocalWeatherData", in: context)
        let transc = NSManagedObject(entity: entity!, insertInto: context)
        
        transc.setValue("\(lat)", forKey: "lattitude")
        transc.setValue("\(long)", forKey: "longtitude")
        transc.setValue("\(city)", forKey: "city")
        transc.setValue("\(country)", forKey: "country")
        transc.setValue(temp, forKey: "temperature")
        
        //save the object
        do {
            try context.save()
            completion()
        } catch {
            failure()
        }
    }
}
