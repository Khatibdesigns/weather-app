//
//  DataTableViewCell.swift
//  Weather - Nader
//
//  Created by Khatib Designs on 2/9/19.
//  Copyright © 2019 Khatib Designs. All rights reserved.
//

import UIKit

class DataTableViewCell: UITableViewCell {

    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var weatherValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func ConfigureCell(label: String, value: String) {
        self.weatherLabel.text! = label
        self.weatherValueLabel.text! = value
    }
}
