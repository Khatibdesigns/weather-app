//
//  DetailsViewController.swift
//  Weather - Nader
//
//  Created by Khatib Designs on 2/9/19.
//  Copyright © 2019 Khatib Designs. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class DetailsViewController: UIViewController, NVActivityIndicatorViewable {
    
    
    var weatherData = [
        ["label":"",
         "value":""],
        ["label":"",
         "value":""],
        ["label":"",
         "value":""],
        ["label":"",
         "value":""],
        ["label":"",
         "value":""],
        ["label":"",
         "value":""],
        ["label":"",
         "value":""],
        ["label":"",
         "value":""],
        ["label":"",
         "value":""]]
    
    var resusableID = "DataTableViewCell"
    var lat = ""
    var long = ""
    
    @IBOutlet weak var SelectedCityLabel: UILabel!
    @IBOutlet weak var WeatherDatatableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        getTemp()
    }
    
    func configureView() {
        
        self.WeatherDatatableView.delegate = self
        self.WeatherDatatableView.dataSource = self
        self.WeatherDatatableView.register(UINib(nibName: "\(resusableID)", bundle: nil), forCellReuseIdentifier: "\(resusableID)")
    }
    
    // Get Weather Data For Geo
    func getTemp() {
        self.startAnimating()
        
        let param = [
            "long":"\(long)",
            "lat":"\(lat)"
        ]
        APIClient().localWeather(withParameters: param, completion: { (data) in
            
            self.weatherData = [
                ["label":"Sunrise",
                 "value":"\(data[0].sunrise)"],
                ["label":"Sunset",
                 "value":"\(data[0].sunset)"],
                ["label":"Humidity",
                 "value":"\(data[0].humidity)"],
                ["label":"Pressure",
                 "value":"\(data[0].pressure)"],
                ["label":"Wind Speed",
                 "value":"\(data[0].wind_speed)"],
                ["label":"Longtitude",
                 "value":"\(data[0].longtitude)"],
                ["label":"Latitude",
                 "value":"\(data[0].latitude)"],
                ["label":"Temperature",
                 "value":"\(data[0].temperature)"],
                ["label":"City",
                 "value":"\(data[0].city)"],
                ["label":"Country",
                 "value":"\(data[0].country)"]]
            
            self.SelectedCityLabel.text! = "\(data[0].city), \(data[0].country)"
            self.WeatherDatatableView.reloadData()
            self.stopAnimating()
        }) {
            self.stopAnimating()
        }
    }
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(resusableID)", for: indexPath) as! DataTableViewCell
        
        cell.ConfigureCell(label: "\(weatherData[indexPath.row]["label"]!)",
            value: "\(weatherData[indexPath.row]["value"]!)")
        
        return cell
    }
}
