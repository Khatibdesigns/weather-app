//
//  MainViewController.swift
//  Weather - Nader
//
//  Created by Khatib Designs on 2/9/19.
//  Copyright © 2019 Khatib Designs. All rights reserved.
//

import UIKit
import MapKit
import NVActivityIndicatorView

class MainViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var WeatherMapView: MKMapView!
    @IBOutlet weak var CurrentLocationLabel: UILabel!
    @IBOutlet weak var CurrentTempLabel: UILabel!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var selected_lat = ""
    var selected_long = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let city = UserDefaults.standard.value(forKey: "city") ?? ""
        fetchWeatherData(city: "\(city)")
        configureView()
    }
    
    func configureView() {
        
        let buttonItem = MKUserTrackingBarButtonItem(mapView: WeatherMapView)
        self.navigationItem.rightBarButtonItem = buttonItem
        self.title = "Weather Map"
        
        self.locManager.delegate = self
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locManager.requestWhenInUseAuthorization()
        self.locManager.startUpdatingLocation()
        
        locManager.requestWhenInUseAuthorization()
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.didDoubleTapMap(sender:)))
        doubleTap.delegate = self
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        WeatherMapView.addGestureRecognizer(doubleTap)
    }
    
    func fetchWeatherData(city: String) {
        
        self.startAnimating()
        
        let param = [
            "city" : "\(city)"
        ]
        APIClient().offlineLocalWeather(withParameters: param, completion: { (data) in
            
            self.CurrentTempLabel.text! = "\(data[0].temperature)°"
            self.stopAnimating()
        }) {
            self.stopAnimating()
        }
    }
    
    // Double Tap Map
    @objc func didDoubleTapMap(sender: UITapGestureRecognizer) {
        
        let Annotations = self.WeatherMapView.annotations
        self.WeatherMapView.removeAnnotations(Annotations)
        
        let location = sender.location(in: WeatherMapView)
        let coordinate = WeatherMapView.convert(location,toCoordinateFrom: WeatherMapView)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        WeatherMapView.addAnnotation(annotation)
        self.selected_long = "\(annotation.coordinate.longitude)"
        self.selected_lat = "\(annotation.coordinate.latitude)"
        self.performSegue(withIdentifier: "showLocationSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLocationSegue" {
            let DesVC = segue.destination as! DetailsViewController
            DesVC.lat = selected_lat
            DesVC.long = selected_long
        }
    }
    
    // Fetch City From Geo
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in completion(
            placemarks?.first?.locality,
            placemarks?.first?.country,
            error)
        }
    }
    
    // Get Weather Data For Geo
    func getTemp(lat: String, long: String) {
        
        self.startAnimating()
        
        let param = [
            "long":"\(long)",
            "lat":"\(lat)"
        ]
        APIClient().localWeather(withParameters: param, completion: { (data) in
            
            self.CurrentTempLabel.text! = "\(data[0].temperature)°"
            self.writeTempToCore(
                long: "\(data[0].longtitude)",
                lat: "\(data[0].latitude)",
                city: "\(data[0].city)",
                country: "\(data[0].country)",
                temp: data[0].temperature)
            self.stopAnimating()
        }) {
            self.stopAnimating()
        }
    }
    
    // Write Temp To Core
    func writeTempToCore(long: String, lat: String, city: String, country: String, temp: Double) {
        
        self.startAnimating()
        
        let param: [String: Any] = [
            "long":"\(long)",
            "lat":"\(lat)",
            "city":"\(city)",
            "country":"\(country)",
            "temp": temp
        ]
        
        APIClient().writeOfflineLocalWeather(withParameters: param, completion: {
            self.stopAnimating()
        }) {
            self.alert(message: "\(Err_Later)", title: "\(Title)")
            self.stopAnimating()
        }
    }
}

extension MainViewController: CLLocationManagerDelegate {
    
    // Edit Annotation Icon
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuseID = "Current Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        if annotationView == nil {
            let pin = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            pin.image = #imageLiteral(resourceName: "pin")
            pin.isEnabled = true
            pin.canShowCallout = true
            
            annotationView = pin
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        view.image = #imageLiteral(resourceName: "pin")
    }
    
    // Update Current Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let lat = location.coordinate.latitude
            let long = location.coordinate.longitude
            
            let location = CLLocation(latitude: lat, longitude: long)
            self.fetchCityAndCountry(from: location) { city, country, error in
                guard let city = city, let country = country, error == nil else { return }
                self.CurrentLocationLabel.text! = "\(city), \(country)"
                UserDefaults.standard.set("\(city)", forKey: "city")
            }
            self.getTemp(lat: "\(lat)", long: "\(long)")
            let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.WeatherMapView.setRegion(region, animated: true)
        }
    }
}

extension MainViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
