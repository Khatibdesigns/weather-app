//
//  MainNavigationViewController.swift
//  Weather - Nader
//
//  Created by Khatib Designs on 2/9/19.
//  Copyright © 2019 Khatib Designs. All rights reserved.
//

import UIKit

class MainNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationBar.barTintColor = UIColor(hex: "4693BB")
        self.navigationBar.dropShadow()
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationBar.isTranslucent = false
    }
}
