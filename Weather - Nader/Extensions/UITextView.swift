//
//  UITextView.swift
//  WCS
//
//  Created by Khatib Designs on 11/3/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation
import UIKit


extension UITextView {
    
    func setborderandradius(_ radius: CGFloat, _ color: String) {
        
        self.layer.borderColor = UIColor(hex: "\(color)").cgColor
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 1
        
    }
}
