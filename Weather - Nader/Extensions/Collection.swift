//
//  Collection.swift
//  WCS
//
//  Created by Khatib Designs on 11/3/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
