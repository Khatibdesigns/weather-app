//
//  Definitions.swift
//  WCS
//
//  Created by Iva Hlebarova on 11/13/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation
import UIKit


var Err = "Error"
var Err_Later = "Please Try Again Later"
var Err_Contact_Adm = "Please Contact The Administrator"
var Err_Empty = "All Fields Must Be Filled"
var Title = "Weather"
var Success = "Operation Succesfull"
var Wrong_Pass = "Wrong Email/Password Combination"

struct attachFileList {
    var docName = String()
    var docURL = String()
}

struct Platform {
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}
