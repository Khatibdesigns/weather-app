//
//  UIView.swift
//  WCS
//
//  Created by Khatib Designs on 11/3/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setborderandraduis(_ radius: CGFloat, _ color: String) {
        
        if color != "" {
            self.layer.borderColor = UIColor(hex: "\(color)").cgColor
            self.layer.borderWidth = 1
        }
        self.layer.cornerRadius = radius
    }
    
    func dropShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 8
    }
    
    func pinSubview(_ subview:UIView, toEdge edge:NSLayoutConstraint.Attribute, withConstant constant:Float) {
        self.pinSubviews(self, subview2: subview, toEdge: edge, withConstant: constant)
    }
    
    func pinSubviews(_ subview1:UIView, subview2:UIView, toEdge edge:NSLayoutConstraint.Attribute, withConstant constant:Float) {
        pin(firstSubview: subview1, firstEdge: edge, secondSubview: subview2, secondEdge: edge, with: constant)
    }
    
    func pin(firstSubview subview1:UIView, firstEdge edge1:NSLayoutConstraint.Attribute, secondSubview subview2:UIView, secondEdge edge2:NSLayoutConstraint.Attribute, with constant:Float) {
        let constraint = NSLayoutConstraint(item: subview1, attribute: edge1, relatedBy: .equal, toItem: subview2, attribute: edge2, multiplier: 1, constant: CGFloat(constant))
        self.addConstraint(constraint)
    }
    
    func pinSubview(_ subview:UIView, withHeight height:CGFloat) {
        let height = NSLayoutConstraint(item: subview, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
        self.addConstraint(height)
    }
    
    func pinSubview(_ subview:UIView, withWidth width:CGFloat) {
        let width = NSLayoutConstraint(item: subview, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width)
        self.addConstraint(width)
    }
    
}
