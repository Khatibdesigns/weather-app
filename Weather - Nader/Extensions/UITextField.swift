//
//  UITextField.swift
//  WCS
//
//  Created by Khatib Designs on 11/3/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation
import UIKit

private let textFieldHorizontalMargin: CGFloat = 16.5
private let textFieldHeight: CGFloat = 50

extension UITextField {
    
    func setborderandradius(_ radius: CGFloat, _ color: String) {
    
    self.layer.borderColor = UIColor(hex: "\(color)").cgColor
    self.layer.cornerRadius = radius
    self.layer.borderWidth = 1
    }
    
    func setpadding() {
        
        let frame = CGRect(x: 0, y: 0, width: textFieldHorizontalMargin, height: textFieldHeight)
        self.leftView = UIView(frame: frame)
        self.leftViewMode = .always
        
        self.rightView = UIView(frame: frame)
        self.rightViewMode = .always
    }
}
