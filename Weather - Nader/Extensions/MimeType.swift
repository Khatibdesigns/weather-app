//
//  MimeType.swift
//  SafeSync Pro
//
//  Created by Iva Hlebarova on 12/6/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation
import MobileCoreServices

func mimeTypeForPath(path: String) -> String {
    let url = NSURL(fileURLWithPath: path)
    let pathExtension = url.pathExtension
    
    if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
        if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
            return mimetype as String
        }
    }
    return "application/octet-stream"
}

func mediaTypeFromURL(url: String) -> String {
    var mimeType = mimeTypeForPath(path: url)
    let exp_mimeType = mimeType.components(separatedBy: "/")
    mimeType = exp_mimeType[0]
        return "\(mimeType)"
}
