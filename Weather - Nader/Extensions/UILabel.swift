//
//  UILabel.swift
//  WCS
//
//  Created by Khatib Designs on 11/3/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func halfTextColorChange (fullText : String , changeText : String, color: String) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)//NSForegroundColorAttributeName
        attribute.addAttribute(NSAttributedString.Key.foregroundColor , value: UIColor(hex: "\(color)") , range: range)
        self.attributedText = attribute
    }
}
