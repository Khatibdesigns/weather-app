//
//  NSArray.swift
//  Safe Sync Pro
//
//  Created by Iva Hlebarova on 12/19/18.
//  Copyright © 2018 Khatib Designs. All rights reserved.
//

import Foundation

extension Array {
    
    // Safely lookup an index that might be out of bounds,
    // returning nil if it does not exist
    func get(index: Int) -> Element? {
        if 0 <= index && index < count {
            return self[index]
        } else {
            return nil
        }
    }
}
